import queue
import threading

from snowflake.snowpark import Session, dataframe
from src.data_processing.utils.logr import sf_logger, fw_logger
import mparticle
import concurrent.futures
from src.data_processing.core.mp_sink_utils \
    import flush_failed_records, get_total_failed_rec_count, send_data, build_pay_load


# Define the DDL function that executes a query or a list of queries
def ddl(ss: Session, func_args: dict, df_dict: dict = None) -> dict:
    """
    This function executes a single query or a list of queries using Snowflake Snowpark.

    Parameters:
    ss (Session): The Snowflake Snowpark session object.
    func_args (dict): A dictionary of arguments for the DDL function.
    df_dict (dict, optional): A dictionary of dataframes. This argument is not used in this function.

    Returns:
    None
    """
    fw_logger.info(f"Executing `ddl` with {func_args}")
    try:
        # Check if the query is present in the function arguments
        if 'query' in func_args:
            # Execute the query and collect the results
            ss.sql(func_args['query']).collect()
            sf_logger.info(f"Executed query: {func_args['query']}")
        # Check if the query_list is present in the function arguments
        elif 'query_list' in func_args:
            # Loop through the query_list and execute each query
            for query in func_args['query_list']:
                ss.sql(query).collect()
                sf_logger.info(f"Executed query: {query}")
        # If neither query nor query_list is present, raise an error
        else:
            log_msg = "Invalid arguments for the DDL function, only `query` or `query_list` supported"
            fw_logger.error(log_msg)
            raise ValueError(log_msg)
    except Exception as e:
        log_msg = f'Failure in `ddl` function with {e}'
        fw_logger.error(log_msg)
        raise Exception(log_msg)

    return df_dict


# Define the function that consumes the stream data
def consume_stream(ss: Session, func_args: dict, df_dict: dict) -> dict:
    """
    This function consumes data from a stream in Snowflake.

    Parameters:
    ss (Session): The Snowflake Snowpark session object.
    func_args (dict): A dictionary of arguments for the consume_stream function.
    df_dict (dict): A dictionary of dataframes.

    Returns:
    dict: The updated df_dict with the additional key "stream_has_data".
    """
    fw_logger.info(f"Executing `consume_stream` with {func_args}")
    try:
        if df_dict.get('restart_flag'):
            sf_logger.info(f'processing the records from previous failed run')
        else:
            # Check if the stream has data
            data_exist_flag = \
                ss.sql(f"select SYSTEM$STREAM_HAS_DATA('{func_args['stream_name']}') AS STREAM_FLAG").first()
            if data_exist_flag['STREAM_FLAG']:
                # Execute the stream query and collect the results
                ss.sql(func_args['stream_query']).collect()
                sf_logger.info(f"Consumed stream data using the query: {func_args['stream_query']}")
                df_dict.update({"stream_has_data": True})
            else:
                # If the stream has no data, print a message indicating that further steps should be skipped
                fw_logger.warning("Source is empty. Skipping further steps")
                df_dict.update({"stream_has_data": False})
    except Exception as e:
        log_msg = f'Failure in `consume_stream` function with {e}'
        fw_logger.error(log_msg)
        raise Exception(log_msg)

    return df_dict


# Define the SQL function that executes a query and returns the output dataframe
def sql(ss: Session, func_args: dict, df_dict: dict) -> dict:
    """
    Executes an SQL query and returns the output dataframe.

    Parameters:
    - ss (Session): The Snowflake Snowpark session instance.
    - func_args (dict): A dictionary containing the following keys:
        - 'query': The SQL query to be executed.
        - 'output_df': The name of the output dataframe.
    - df_dict (dict): A dictionary of existing dataframes.

    Returns:
    - dict: A dictionary of existing dataframes including the output of the executed SQL query.
    """
    fw_logger.info(f"Executing `sql` with {func_args}")
    try:
        # Execute the query and store the result in a dataframe
        stage_output_df = ss.sql(func_args['query'])
        stage_output_df.show()
        # Update the df_dict with the new dataframe
        df_dict.update({func_args['output_df']: stage_output_df})
        # Log the result of the SQL query
        sf_logger.info(f"Executed query: {func_args['query']}, Output dataframe: {func_args['output_df']}")
    except Exception as e:
        log_msg = f'Failure in `sql` function with {e}'
        fw_logger.error(log_msg)
        raise Exception(log_msg)
    return df_dict


def mp_sink(ss: Session, func_args: dict, df_dict: dict) -> dict:
    """
    Sends data from a Snowflake DataFrame to mParticle.

    This function takes a `Session` object, a dictionary of arguments `func_args`, and a dictionary of DataFrames
    `df_dict` as input. It returns a dictionary of results.

    Args: _: Session: A `Session` object that represents a connection to a Snowflake account. func_args (dict): A
    dictionary of arguments that contains configuration information for the function. df_dict (dict): A dictionary of
    DataFrames, where each key is a string that represents the name of the DataFrame, and the value is a DataFrame.

    Returns:
        dict: A dictionary of results.
    """

    # Get the API key and API secret from AWS secret manager

    fw_logger.info(f"Executing `mparticle sink` with {func_args}")
    sf_logger.info('Started executing mparticle function')

    mp_conf = df_dict.get("mparticle", {})

    failed_rec_table = func_args['mp_properties']['fail_rec_table_name']

    if df_dict.get('restart_flag') and int(df_dict.get('restart_step_id')) == int(func_args['step_id']):
        restart_run_id = df_dict.get('restart_run_id')
        source_df_xform: dataframe = \
            ss.table(failed_rec_table)\
            .where(f"RUN_ID = '{restart_run_id}' and (STATUS_CODE = 429 or STATUS_CODE = 999 or STATUS_CODE = 400 or STATUS_CODE like '5%')")
        restart_from_mp_sink = True
    else:
        restart_from_mp_sink = False
        # Get the source dataframe
        source_df: dataframe = df_dict[func_args['input_df']]
        source_df_xform: dataframe = source_df.selectExpr(func_args['select_exp'])

    # Set the environment to development
    environment = mp_conf.get('environment')

    # Uncomment the following code assigning from mp_properties
    # context = mparticle.DataPlanContext()
    # context.plan_id = func_args['mp_properties']['plan_id']
    # context.plan_version = func_args['mp_properties']['plan_version']
    # batch.context = context

    # Create a Configuration object
    configuration = mparticle.Configuration()

    # Set API key and API secret for mParticle
    # secret_dict = get_secret('secret_name')
    # configuration.api_key = secret_dict.get(func_args['mp_properties']['secret_name'])
    # configuration.api_secret = secret_dict.get('api_secret')
    configuration.api_key = mp_conf.get('api_key', '')
    configuration.api_secret = mp_conf.get('api_secret', '')
    configuration.connection_pool_size = int(mp_conf.get('connection_pool_size', '1'))
    configuration.debug = mp_conf.get('debug_enabled', 'false').lower() == 'true'

    # Create an EventsApi object using the Configuration object
    api_instance = mparticle.EventsApi(configuration)

    instance_run_id = df_dict['run_id']

    # Exponential backoff pattern configuration
    exp_backoff_conf = mp_conf.get("exp_backoff", {})
    default_max_retries = exp_backoff_conf.get("max_retries", 5)
    default_base_wait_time = exp_backoff_conf.get("base_wait_timee", 2)
    default_max_wait_time = exp_backoff_conf.get("max_wait_time", 60)

    max_queue_size = mp_conf.get("failed_record_queue_size", 50)

    # Create a queue for failed records
    failed_records = queue.Queue()
    # Create a queue to store the counts
    count_queue = queue.Queue()
    # create a lock object
    lock = threading.Lock()

    max_threads = mp_conf.get("max_workers", 25)

    # Initialize record count
    record_count = 0

    with concurrent.futures.ThreadPoolExecutor(max_workers=max_threads) as executor:
        futures = []
        try:
            for batch_source_df in source_df_xform.to_pandas_batches():
                for _, rec in batch_source_df.iterrows():
                    record_count += 1
                    payload = build_pay_load(environment, func_args['column_mapping'], rec, restart_from_mp_sink)
                    if record_count == 1:  # first record to handle auth exception
                        try:
                            send_data(ss=ss,
                                      data=payload,
                                      api_inst=api_instance,
                                      max_retries=default_max_retries,
                                      base_wait_time=default_base_wait_time,
                                      max_wait_time=default_max_wait_time,
                                      run_id=instance_run_id,
                                      failed_table_name=failed_rec_table,
                                      failed_records=failed_records,
                                      count_queue=count_queue,
                                      lock=lock,
                                      batch_size=max_queue_size)
                        except Exception as e:
                            raise Exception(e)
                    else:
                        futures.append(executor.submit(send_data,
                                                       ss,
                                                       payload,
                                                       api_instance,
                                                       default_max_retries,
                                                       default_base_wait_time,
                                                       default_max_wait_time,
                                                       instance_run_id,
                                                       failed_rec_table,
                                                       failed_records,
                                                       count_queue,
                                                       lock,
                                                       max_queue_size))

                for future in concurrent.futures.as_completed(futures):
                    if future.exception() is not None:
                        exception = future.exception()
                        sf_logger.error('mparticle sink function has been failed')
                        executor.shutdown(wait=False)
                        import traceback
                        traceback.print_exc()
                        failed_rec_count = get_total_failed_rec_count(count_queue=count_queue)
                        sf_logger.info(f"Total processed records {record_count} and "
                                       f"failed record count: {failed_rec_count}")
                        raise Exception(f"Failed to send data to mParticle with exception: {exception}")
            else:
                executor.shutdown(wait=False)
                flush_failed_records(ss, failed_rec_table, failed_records, count_queue, lock)
                failed_rec_count = get_total_failed_rec_count(count_queue=count_queue)
                sf_logger.info(
                    f'Successful completion of the mparticle sink function. Total records processed: {record_count} and '
                    f'failed record count: {failed_rec_count}'
                )
        except Exception as e:
            raise Exception (f"Data processing failed while sending to mparticle with following {e}")

    # Update the df_dict with total record count and failed record count
    df_dict.update({
        'total_records_processed': record_count,
        'failed_record_count': failed_rec_count
    })
    return df_dict
