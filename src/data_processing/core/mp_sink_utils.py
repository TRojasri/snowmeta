import json
from snowflake.snowpark import Session
from src.data_processing.utils.logr import sf_logger
import mparticle
import time
import random
import queue
from datetime import datetime
from urllib3.exceptions import ProtocolError


def flush_failed_records(ss: Session,
                         sf_table_name: str,
                         failed_records: queue.Queue,
                         count_queue: queue.Queue,
                         lock):
    """
    Flushes the contents of the failed records queue to a snowflake table. The queue is consumed
    in batches of up to `batch_size` records, and each batch is inserted into the database
    as a separate transaction.

    This function should be called periodically to ensure that failed records are processed
    and stored in the database.

    Returns:
        None.
    """
    # acquire the lock before accessing the queue
    lock.acquire()
    failed_records_batch = []
    sf_logger.info("Consuming failed records queue")

    while not failed_records.empty():
        try:
            failed_records_batch.append(failed_records.get_nowait())
        except queue.Empty:
            break

    if failed_records_batch:
        failed_record_count = len(failed_records_batch)
        sf_logger.info(f"Flushed {failed_record_count} failed records from the queue")
        count_queue.put(failed_record_count)
        sf_logger.info("Inserting failed records into the Snowflake table")
        try:
            failed_records_df = ss.create_dataframe(failed_records_batch,
                                                    ['request_date',
                                                     'request_ts',
                                                     'run_id',
                                                     'status_code',
                                                     'failed_reason',
                                                     'source_request_id',
                                                     'data'])
            failed_records_df.write.mode('append').saveAsTable(sf_table_name)  # get this from configuration
            sf_logger.info(
                f"Inserted {failed_record_count} failed records "
                f"from batch {count_queue.qsize()} into the Snowflake table {sf_table_name}")
        except Exception as fail_rec_e:
            sf_logger.error(
                f"Snowflake insert failed with {fail_rec_e} while writing failed records "
                f"into the Snowflake table {sf_table_name}")
            raise Exception(fail_rec_e)
        finally:
            failed_records_batch.clear()
            # release the lock after the queue has been flushed
            lock.release()


# Get the counts from the queue and calculate the final count
def get_total_failed_rec_count(count_queue: queue.Queue):
    """
    Retrieves the total number of failed records that have been processed by the `flush_failed_records` function.

    This function should be called at the end to get number of failed records that have been processed stored
    in the snowflake table.

    Returns:
        An integer representing the total number of failed records that have been processed.
    """
    sf_logger.info("Retrieving failed records queue")
    total_count = 0
    while not count_queue.empty():
        count = count_queue.get()
        total_count += count
    return total_count


def send_data(ss,
              data,
              api_inst,
              max_retries,
              base_wait_time,
              max_wait_time,
              run_id,
              failed_table_name,
              failed_records,
              count_queue,
              lock,
              batch_size,
              callback=None):
    """
    Sends data to an API using the given API client, with support for retrying failed requests
    and batching data into smaller chunks.

    Args:
        ss: Snowpark Session
        data: A list of dictionaries representing the data to send to the API.
        api_inst: An instance of the API client to use for sending the data.
        run_id: A string identifier for the current run.
        failed_table_name: A snowflake to store failed records
        failed_records: An queue used to store failed records for later processing.
        max_retries: The maximum number of times to retry a failed request.
        batch_size: The maximum number of records to send in a single request.
        base_wait_time: The base wait time (in seconds) between retry attempts.
        max_wait_time: The maximum wait time (in seconds) between retry attempts.
        count_queue: An queue used to store failed records batch count.
        lock: lock object for synchronization
        callback: An optional function to call after each successful request.

    Returns:
        None.
    """
    # Get the current date
    now = datetime.now()
    current_date = now.strftime('%Y-%m-%d')

    def wait_before_retry(wait_tm):
        total_wait_time = wait_tm + random.random()
        sf_logger.warning(f"Retry after {round(total_wait_time, 2)} seconds.")
        time.sleep(total_wait_time)

    # Function to add failed records to the queue
    def add_failed_record(error_status, error_reason=None):
        # Format the current date and time as a string
        timestamp_str = now.strftime('%Y-%m-%d %H:%M:%S.%f')
        failed_records.put((current_date,
                            timestamp_str,
                            run_id,
                            error_status,
                            error_reason,
                            data.user_identities.email,
                            data.to_dict())
                           )
        if failed_records.qsize() == batch_size:
            flush_failed_records(ss, failed_table_name, failed_records, count_queue, lock)
        sf_logger.warning("Maximum retries reached. Record added to failed records queue")

    num_retries = 0
    wait_time = base_wait_time

    while num_retries < max_retries:
        try:
            response = api_inst.upload_events(data, callback=callback)
            body, status_code, headers = response
            if status_code == 200 or status_code == 202:
                return response
            else:
                raise Exception(response)
        except ProtocolError as send_data_e:
            sf_logger.warning(f"Protocol error occurred: {send_data_e}. Retrying...")

            num_retries += 1

            if num_retries == max_retries:
                add_failed_record(999, 'Protocol error')
                return send_data_e
            else:
                sf_logger.warning(f"Retry attempt {num_retries} of {max_retries}")

            wait_before_retry(min(wait_time * 2, max_wait_time))

        except Exception as send_data_e:
            if send_data_e.status == 429:
                sf_logger.warning(f"mParticle API rate limit exceeded : {send_data_e.status}")

                num_retries += 1

                if num_retries == max_retries:
                    add_failed_record(send_data_e.status, send_data_e.reason)
                    return send_data_e.status
                else:
                    sf_logger.warning(f"Retry attempt {num_retries} of {max_retries}")

                retry_after = int(send_data_e.headers.get('Retry-After', '0'))

                if retry_after > 0:
                    wait_before_retry(retry_after)
                else:
                    wait_before_retry(min(wait_time * 2, max_wait_time))
            elif send_data_e.status // 100 == 5:
                # Server error
                sf_logger.warning(f"Received a {send_data_e.status} error.")

                num_retries += 1

                if num_retries == max_retries:
                    add_failed_record(send_data_e.status, send_data_e.reason)
                    return send_data_e.status
                else:
                    sf_logger.warning(f"Retry attempt {num_retries} of {max_retries}")

                wait_before_retry(min(wait_time * 2, max_wait_time))
            elif send_data_e.status == 400:
                # Bad request
                sf_logger.warning(f"Received a {send_data_e.status} error : {send_data_e.reason}. "
                                  f"Adding the failed record to the queue.")
                add_failed_record(send_data_e.status, send_data_e.reason)

                return send_data_e.status
            else:
                # Unhandled exception
                raise Exception(f"Received a {send_data_e} error")


# Build the payload for mParticle API
def build_pay_load(environment, column_mapping, record, restart_mp_sink_flag):

    if not restart_mp_sink_flag:
        mbatch = mparticle.Batch()
        mbatch.environment = environment

        # User Identities
        """
        Data be associated with a user identity
        """
        if 'user_identities' in column_mapping:
            # Build the user identities from the record and column mapping
            identities = {target_field: record[str(src_field).upper()]
                          for src_field, target_field in column_mapping['user_identities'].items()}
            mbatch.user_identities = mparticle.UserIdentities(**identities)
            sf_logger.debug(f'User identities built: {mbatch.user_identities}')

        # User attributes
        """
        The mParticle audience platform can be powered by only sending a combination of user attributes, 
        used to describe segments of users, and device identities/user identities used to then target those users.
        """
        if 'user_attributes' in column_mapping:
            user_attributes = {}
            for source_fields, target_field in column_mapping['user_attributes'].items():
                user_attributes[target_field] = record[source_fields.upper()]
            mbatch.user_attributes = user_attributes
            sf_logger.debug(f'User attributes built: {mbatch.user_attributes}')

        # Device info
        """
        The DeviceInformation object describes a mobile device that should be associated with this batch. Crucially, 
        it exposes properties for device identities (Apple IDFA and Google Advertising ID) which are required 
        for nearly all mParticle Audience integrations.
        """
        if 'device_info' in column_mapping:
            # Build the device info from the record and column mapping
            device_info_dict = {target_field: record[src_field.upper()]
                                for src_field, target_field in column_mapping['device_info'].items()}
            mbatch.device_info = mparticle.DeviceInformation(**device_info_dict)
            sf_logger.debug(f'Device info built: {mbatch.device_info}')

        # CCPA
        if 'ccpa_consent_state' in column_mapping:
            # Build the CCPA consent state from the record and column mapping
            ccpa_consent_state_dict = {target_field: record[src_field.upper()]
                                       for src_field, target_field in column_mapping['ccpa_consent_state'].items()}
            ccpa_consent_state = mparticle.CCPAConsentState(**ccpa_consent_state_dict)
            sf_logger.debug(f'CCPA consent state built: {ccpa_consent_state}')

            # Add the CCPA consent state to the consent state object
            consent_state_ccpa = mparticle.ConsentState()
            consent_state_ccpa.ccpa = {'data_sale_opt_out': ccpa_consent_state}
            sf_logger.debug(f'Consent state CCPA built: {consent_state_ccpa}')

            mbatch.consent_state = consent_state_ccpa

        # GDPR
        if 'gdpr_consent_state' in column_mapping:
            # Create a dictionary for GDPR consent state fields mapping
            gdpr_consent_state_dict = {target_field: record[src_field.upper()]
                                       for src_field, target_field in column_mapping['gdpr_consent_state'].items()}
            # Create GDPRConsentState object using the created dictionary
            gdpr_consent_state = mparticle.GDPRConsentState(**gdpr_consent_state_dict)
            sf_logger.debug(f'GDPR consent state built: {gdpr_consent_state}')

            consent_state_gdpr = mparticle.ConsentState()
            consent_state_gdpr.gdpr = {'My Purpose': gdpr_consent_state}

            mbatch.consent_state = consent_state_gdpr

        # App Event mapping
        if 'app_event' in column_mapping:
            # Create a dictionary for app event fields mapping
            app_event_dict = {target_field: record[src_field.upper()]
                              for src_field, target_field in column_mapping['app_event'].items()}
            # Create an AppEvent object using the created dictionary
            app_event = mparticle.AppEvent(**app_event_dict)
            # Set the app event object in the events field of the batch object
            mbatch.events.append(app_event)

        # Commerce Event mapping
        """
        The CommerceEvent is central to mParticle’s eCommerce measurement. 
        CommerceEvents can contain many data points but it’s important to understand that there are 3 core variations:
    
        Product-based: Used to measure measured datapoints associated with one or more products
        Promotion-base: Used to measure datapoints associated with internal promotions or campaigns
        Impression-based: Used to measure interactions with impressions of products and product-listings
        """
        commerce_event = []
        if 'product' in column_mapping and 'product_action' in column_mapping:
            # Create a Product object
            product_fields = {target_field: record[src_field.upper()]
                              for src_field, target_field in column_mapping['product'].items()}
            product = mparticle.Product(**product_fields)

            # Create a ProductAction object
            product_action_fields = {target_field: record[src_field.upper()]
                                     for src_field, target_field in column_mapping['product_action'].items()}
            product_action = mparticle.ProductAction(**product_action_fields)
            product_action.products = [product]
            commerce_event += [mparticle.CommerceEvent(product_action)]

            mbatch.events.append(commerce_event)
        else:
            pass

        if 'promotion' in column_mapping and 'promotion_action' in column_mapping:
            # Create a Promotion object
            promotion_fields = {target_field: record[src_field.upper()]
                                for src_field, target_field in column_mapping['promotion'].items()}
            promotion = mparticle.Promotion(**promotion_fields)

            # Create a promotion object
            promotion_action_fields = {target_field: record[src_field.upper()]
                                       for src_field, target_field in
                                       column_mapping['promotion_action'].items()}
            promotion_action = mparticle.PromotionAction(**promotion_action_fields)
            promotion_action.products = [promotion]
            commerce_event += [mparticle.CommerceEvent(promotion_action)]

            mbatch.events.append(commerce_event)
        else:
            pass

        # Session events
        """
        The SessionStartEvent and SessionEndEvent should be used to describe the details of 
        user session such as its length, which is a common metric used in many mParticle integrations. 
        Additonally, length, recency, and frequency of sessions are powerful data-points by which 
        an mParticle audience can be defined.
        """
        if 'session_start' in column_mapping:
            session_start_fields = {target_field: record[src_field.upper()]
                                    for src_field, target_field in column_mapping['session_start'].items()}
            session_start = mparticle.SessionStartEvent(**session_start_fields)

        # Session End Event
        if 'session_end' in column_mapping:
            session_end_fields = {target_field: record[src_field.upper()]
                                  for src_field, target_field in column_mapping['session_end'].items()}
            session_end = mparticle.SessionEndEvent(**session_end_fields)
            session_end.session_id = session_start.session_id  # ensure the session ids match
            session_end.session_duration_ms = session_end_fields.get('SESSION_DURATION_MS', 0)

        if 'session_start' in column_mapping and 'session_end' in column_mapping:
            mbatch.events.append([session_start, session_end])
        elif 'session_start' in column_mapping:
            mbatch.events.append(session_start)
        elif 'session_end' in column_mapping:
            mbatch.events.append(session_end)
    else:
        json_payload = json.loads(record.get('DATA'))
        print(json_payload)
        mbatch = mparticle.Batch(**json_payload)

    return mbatch
