import argparse
import json
import sys

from src.data_processing.utils.logr import fw_logger, sf_logger
from snowflake.snowpark import Session
from datetime import datetime
import botocore
import botocore.session
from aws_secretsmanager_caching import SecretCache, SecretCacheConfig



def get_config(file_name: str) -> dict:
    """
    Reads and returns the content of a JSON configuration file.

    Parameters:
    - file_name (str): The name of the JSON configuration file.

    Returns:
    - dict: The content of the JSON configuration file.
    """
    config_file = open(file_name)
    config = json.load(config_file)
    config_file.close()
    return config


def create_session(conn_prop: dict) -> Session:
    """
    Creates a Snowpark session with configurations specified in `conn_prop`

    Parameters:
    conn_prop (dict): A dictionary of snowflake configurations

    Returns:
    Session: A Snowpark session
    """
    sf_logger.info("Creating a Snowpark Session")
    try:
        ss = Session.builder.configs(conn_prop).create()
    except Exception as e:
        raise Exception(f"Creating Snowpark session failed with {e}")
    return ss


def log_str_size_check(log_str: str) -> str:
    MAX_LOG_SIZE = 16 * 1024 * 1024  # 16 MB in bytes

    if sys.getsizeof(log_str) > MAX_LOG_SIZE:
        return log_str[:MAX_LOG_SIZE]
    else:
        return log_str


def update_job_status(
        ss: Session,
        process_name: str,
        action: str,
        pipeline_config: str = None,
        run_id: int = None,
        restart_run_id: int = None,
        database: str = None,
        schema: str = None,
        pipeline_status: str = None,
        log_msg_str: str = None,
        meta_wh: str = None
        #,meta_role = None
       ):
    """
    Updates the job status into job status history table.

    Parameters:
    - ss (Session): The Snowpark session object.
    - process_name (str): The name of the process.
    - action (str): The action to be taken, either 'insert' or 'update'.
    - resolved_pipeline (str): The resolved pipeline as a JSON string.
    - run_id (str): The run ID of the process.
    - restart_run_id (str): The restart run ID of the process.
    - database (str): The name of the database.
    - schema (str): The name of the schema.
    - pipeline_status (str): The status of the pipeline.
    - log_msg_str (str): The log message of the process.
    - ss_wh (str): The compute warehouse.
    """
    if action == 'insert':
        fw_logger.debug(f"Inserting status into {database}.{schema}.job_status_history")

        restart_run_id = restart_run_id if restart_run_id else 'NULL'

        ss.sql(f'insert into {database}.{schema}.job_status_history '
               f'(PROCESS_NAME, RUN_ID, RESTART_RUN_ID, PIPELINE_CONFIG, STATUS, LOG_MESSAGE, START_TIME, END_TIME) '
               f'select \'{process_name}\', '
               f'{run_id}, '
               f'{restart_run_id}, '
               f'parse_json(\'{pipeline_config}\'),'
               f'\'STARTED\','
               f'null,'
               f'current_timestamp(),'
               f'null').show()
    elif action == 'update':
        fw_logger.debug(f"Updating status into {database}.{schema}.job_status_history")

        # ss.sql(f 'USE ROLE {meta_role}')
        ss.sql(f'USE WAREHOUSE {meta_wh}')
        # log_msg = log_msg_str.replace("'", "\\u0027")
        log_msg = log_msg_str.replace("'", "''")
        ss.sql(f"update {database}.{schema}.job_status_history "
               f"set end_time = current_timestamp(),"
               f"status = '{pipeline_status}',"
               f"log_message = '{log_msg}'"
               f"where run_id = {run_id} "
               f"and process_name = '{process_name}'").show()

    else:
        pass


def send_email(ss: Session, func_args) -> None:
    """
    Sends an email using the specified email integration and email address list.

    Args:
        ss (Session): The Snowpark session object.
        func_args (dict): A dictionary containing the following keys:
            - 'email_intg_name' (str): The name of the email integration.
            - 'email_addr_list' (list): A list of email addresses to send the email to.
            - 'status' (str): The status of the pipeline job ('SUCCEEDED' or 'FAILED').
            - 'process_name' (str): The name of the pipeline job.
            - 'run_id' (str): The ID of the pipeline job run.
            - 'total_records_processed' (int): The total number of records processed by the pipeline job.
            - 'failed_record_count' (int): The number of records that failed to process, if applicable.

    Returns:
        None
    """
    fw_logger.info("Sending email to configured email_list")

    if func_args['status'].upper() == 'SUCCEEDED':
        email_subject = f"Process {func_args['process_name']} has been completed successfully"
        email_body = f"""\
        Hello,

        The pipeline `{func_args["process_name"]}` has completed successfully.

        Start Time: {func_args["start_time"]}
        End Time: {datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
        Run ID: {func_args["run_id"]}
        Total records processed: {func_args["total_records_processed"]}
        Failed record count: {func_args["failed_record_count"]}

        Thank you.
        """

    else:
        email_subject = f"Process {func_args['process_name']} has been failed"
        email_body = f"""Hello,

        The pipeline `{func_args['process_name']}` has failed. 
        Please check the `pipeline` table for more details.

        Start Time: {func_args['start_time']}
        End Time: {datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
        Run ID: {func_args['run_id']}

        Thank you."""

    email_list = '.'.join(func_args['email_addr_list'])

    send_email_expression = f"call system$send_email(" \
                            f"\'{func_args['email_intg_name']}\'," \
                            f"\'{email_list}\'," \
                            f"\'{email_subject}\'," \
                            f"\'{email_body}\')"
    fw_logger.info(email_body)
    ss.sql(send_email_expression).collect()


def get_args():
    """
    Parses command line arguments to get the pipeline and runtime configuration.

    Returns:
        Namespace: A namespace object containing the following attributes:
            process_name (str): The name of the process to run.
            restart_run_id (str, optional): The run ID to restart from. If not provided, the pipeline starts from the
                beginning.
    """
    # Parse command line arguments to get pipeline and runtime config
    parser = argparse.ArgumentParser()

    # Required Arguments
    parser.add_argument('--process_name', type=str, required=True)
    parser.add_argument('--config_file', type=str, required=True)
    parser.add_argument('--restart_step_id', type=int, required=False, default='000')
    parser.add_argument('--restart_run_id', type=int, required=False)
    parser.add_argument('--log_level', default='INFO', help='Log level (default: %(default)s)')
    parser.add_argument('--runtime_params', type=str, required=False)

    # Important testing purpose remove this
    parser.add_argument('--restart_step_id1', type=int, required=False, default='100')
    parser.add_argument('--restart_run_id1', type=int, required=False)

    return parser.parse_args()


def get_secret_from_aws(secret_name: str):
    fw_logger.info("Getting secret from AWS secret manager")
    client = botocore.session.get_session().create_client('secretsmanager')
    cache_config = SecretCacheConfig()
    cache = SecretCache(config=cache_config, client=client)

    secret = cache.get_secret_string(secret_name)

    return secret
