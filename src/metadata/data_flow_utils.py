import json
from snowflake.snowpark.functions import col
from src.metadata.logging_utils import logger
import dynamic_view_query_generation


def set_db_context(object_data, step_id):
    """
    Data flow step to set context.

    Args:

        object_data (dataframe): dataframe of object attributes
        step_id (int):  represents each step with unique value

    Returns:
        dict: steps for setting database context

    """
    db_context = {
        "step_id": step_id,
        "desc": "set the database,schema and warehouse",
        "func": "ddl",
        "query_list": [f"use database {object_data['DATABASE_NAME'].lower()}",
                       f"use schema {object_data['SCHEMA_NAME'].lower()}",
                       f"use warehouse {object_data['COMPUTE_WH_NAME'].lower()}"]
    }
    return db_context


def consume_stream(object_data, step_id):
    """
    Data flow step to consume stream.

    Args:

        object_data (dataframe): dataframe of object attributes
        step_id (int):  represents each step with unique value

    Returns:
        dict: steps for consuming stream

    """
    if object_data['OBJECT_TYPE'].lower() == 'view':
        stream_query = f"create transient table if not exists {object_data['PRIMARY_TABLE'].lower()}_stg " \
                       f"as select * from {object_data['STREAM_NAME']}"
    else:
        stream_query = f"create transient table if not exists {object_data['OBJECT_NAME'].lower()}_stg " \
                       f"as select * from {object_data['STREAM_NAME']}"

    read_stream = {
        "step_id": step_id,
        "desc": "consume stream by loading data into transient table",
        "func": "consume_stream",
        "stream_name": object_data['STREAM_NAME'],
        "stream_query": stream_query
    }
    return read_stream


def create_dynamic_view(session, view_name, primary_table, step_id):
    """
    Data flow step to create dynamic view.

    Args:
        session (Snowpark Session): snowpark session object
        view_name (str): name of the view to be created
        primary_table(str): name of the driving table for view
        step_id (int):  represents each step with unique value

    Returns:
        dict: steps for creating dynamic view

    """
    view_query = dynamic_view_query_generation.create_dynamic_view_query(session, view_name, primary_table)

    dynamic_view_query = {
        "step_id": step_id,
        "desc": "dynamic view query generation",
        "func": "ddl",
        "query": f"{view_query}"
    }
    return dynamic_view_query


def extract_data(object_data, step_id):
    """
    Data flow step to extract data from transient table.

    Args:
        object_data (dataframe): dataframe of object attributes
        step_id (int):  represents each step with unique value

    Returns:
        dict: steps for extracting data

    """
    object_name = object_data['OBJECT_NAME'].lower()
    if object_data['OBJECT_TYPE'].lower() == 'view':
        extract_query = f"select * from {object_name}"
        descr = 'view'
    else:
        if object_data['INCREMENTAL_CAPTURE_FLAG']:
            extract_query = f"select * from {object_name}_stg"
            descr = 'transient table'
        else:
            extract_query = f"select * from {object_name}"
            descr = 'table'

    data_extract = {
        "step_id": step_id,
        "desc": f"create dataframe extracting data from {descr}",
        "func": "sql",
        "query": extract_query,
        "output_df": "incr_df"
    }
    return data_extract


def mp_sink(column_map, select_exp_list, target_prop, step_id):
    """
    Data flow step to construct payload for mparticle.

    Args:
        column_map (dict): dictionary of target columns grouped by  mparticle object categories
        select_exp_list (list): list containing source and target column mapping
        target_prop(dict): dictionary containing target properties
        step_id (int):  represents each step with unique value

    Returns:
        dict: steps for constructing mparticle payload

    """
    load_mp_sink = {
        "step_id": step_id,
        "desc": "convert dataframe to payload and send data to mparticle",
        "func": "mp_sink",
        "input_df": "incr_df",
        "select_exp": select_exp_list,
        "mp_properties": target_prop,
        "column_mapping": column_map
    }
    return load_mp_sink


def drop_transient_table(object_data, step_id):
    """
    Data flow step to Drop the transient table.

    Args:
        object_data (dataframe): dataframe of object attributes
        step_id (int):  represents each step with unique value

    Returns:
        dict: steps to drop transient table

    """

    if object_data['OBJECT_TYPE'].lower() == 'view':
        drop_query = f"drop table {object_data['PRIMARY_TABLE'].lower()}_stg"
    else:
        drop_query = f"drop table {object_data['OBJECT_NAME'].lower()}_stg"

    drop_trans_table = {
        "step_id": step_id,
        "desc": "Drop the transient table",
        "func": "ddl",
        "query": drop_query
    }
    return drop_trans_table


def column_mapping(session, process_name, target):
    """
        Create Column mapping from source to target.

    Args:

        session (Snowpark Session): snowpark session object
        process_name (str): name of the process for which metadata generation is triggered
        target(str): Name of the target

    Returns:
        list: list containing source and target column mapping
        dict: dictionary of target columns grouped by  mparticle object categories

    Raises:
          Exception: if column attributes for corresponding target are not found


    """

    # Get data from column attributes table for matching target
    col_attr_df = session.table('column_attr') \
        .filter(col('process_name') == process_name) \
        .filter(col('TARGET_NAME') == target).collect()

    if col_attr_df:
        column_map = {}
        select_expr = []
        for record in col_attr_df:
            column_attr = record.as_dict()
            if column_attr['TARGET_COLUMN_NAME']:
                source_column_name = column_attr['COLUMN_NAME'].lower()
                transform_rule = column_attr['TRANSFORM_RULE']
                object_category = column_attr['OBJECT_CATEGORY'].lower()
                target_column_name = column_attr['TARGET_COLUMN_NAME']
                column_name = target_column_name + "_" + object_category

                if object_category in column_map.keys():
                    column_map[object_category][column_name] = target_column_name
                else:
                    column_map.update({object_category: {column_name: target_column_name}})

                if transform_rule:
                    select_expr.append(f"{transform_rule} AS {column_name}")
                else:
                    select_expr.append(f"{source_column_name} AS {column_name}")
    else:
        raise Exception(f"Column attributes not found for the process {process_name}, target {target}")

    return column_map, select_expr


def construct_data_flow(session, process_name, object_data):
    """
    Construct dataflow list.

    Args:
        session (Snowpark Session): snowpark session object
        process_name (str): name of the process for which metadata generation is triggered
        object_data (dataframe): dataframe of object attributes]

    Returns:
        list: list containing steps for data flow

    Raises:
          Exception: if target properties for corresponding target are not found

    """
    step_id = 100
    data_flow_list = [set_db_context(object_data, step_id)]

    if object_data['INCREMENTAL_CAPTURE_FLAG']:
        step_id = step_id + 100
        data_flow_list.append(consume_stream(object_data, step_id))
    else:
        pass

    if object_data['OBJECT_TYPE'].lower() == 'view':
        step_id = step_id + 100
        data_flow_list.append(
            create_dynamic_view(session, object_data['OBJECT_NAME'], object_data['PRIMARY_TABLE'], step_id))
    else:
        pass

    step_id = step_id + 100
    data_flow_list.append(extract_data(object_data, step_id))

    targets_list = json.loads(object_data['TARGET_NAME_LIST'])
    if len(targets_list) == 0:
        raise Exception(f"Target name list is empty in object attributes table."
                        f" Please provide minimum one target name.")

    for target in targets_list:
        if target.lower() == 'mparticle':
            # get data from target attributes table
            target_attr = session.table('target_attr') \
                .filter(col('process_name') == process_name) \
                .filter(col('TARGET_NAME') == target).first()

            if target_attr:
                target_attr_dict = target_attr.as_dict()
                logger.info(f"Target attributes retrieved for process {process_name}, target {target}")
                if target_attr_dict.get('TARGET_PROPERTIES'):
                    target_prop = json.loads(target_attr_dict['TARGET_PROPERTIES'])
                else:
                    raise Exception(f"Target properties not provided for process {process_name}, target {target}")
            else:
                raise Exception(f"Target attributes not found for process {process_name}, target {target}")

            col_map, select_exp_list = column_mapping(session, process_name, target)
            step_id = step_id + 100
            data_flow_list.append(mp_sink(col_map, select_exp_list, target_prop, step_id))
        else:
            raise Exception(f'Invalid target {target}.Only mparticle is supported currently')

    step_id = step_id + 100
    data_flow_list.append(drop_transient_table(object_data, step_id))
    return data_flow_list


def construct_pipeline_json(session, process_name, object_data):
    # Construct data flow json
    data_flow = construct_data_flow(session, process_name, object_data)

    logger.info(f"Data Flow JSON successfully created for process '{process_name}'")

    # Get data from email attributes table if configured in object_attr
    email_intg_name, email_addr_list = None, None
    if object_data.get('EMAIL_INTG_NAME'):
        email_attr = session.table('email_attr') \
            .filter(col('email_intg_name') == object_data['EMAIL_INTG_NAME']).first().as_dict()
        email_intg_name = email_attr['EMAIL_INTG_NAME']
        email_addr_list = json.loads(email_attr['EMAIL_ADDR_LIST'])
        logger.info(f"Retrieved email attributes for process '{process_name}'")
    else:
        logger.info(f"No email integration configured for process '{process_name}'")

    # construct pipeline JSON
    pipeline_metadata = {
        "root": {
            "pipeline_name": object_data['PROCESS_NAME'],
            "desc": object_data['PROCESS_DESC'] if object_data['PROCESS_DESC'] else None,
            "email_intg_name": email_intg_name,
            "email_addr_list": email_addr_list,
            "data_flow": data_flow
        }
    }
    pipeline_metadata_json = json.dumps(pipeline_metadata)

    return pipeline_metadata_json
